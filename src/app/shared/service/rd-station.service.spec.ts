import { TestBed } from '@angular/core/testing';

import { RdStationService } from './rd-station.service';

describe('RdStationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RdStationService = TestBed.get(RdStationService);
    expect(service).toBeTruthy();
  });
});
