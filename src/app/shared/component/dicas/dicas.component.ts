import { Component, OnInit } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-dicas',
  templateUrl: './dicas.component.html',
  styleUrls: ['./dicas.component.scss']
})
export class DicasComponent implements OnInit {

  dicas;

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.lb.getPosts(2)
    .subscribe(data => {
      this.dicas = data;
      console.log(this.dicas);
    });
  }

}
