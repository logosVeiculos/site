import { Component, OnInit, Input } from '@angular/core';
import { LoopbackService } from '../../service/loopback.service';

@Component({
  selector: 'app-opcion',
  templateUrl: './opcion.component.html',
  styleUrls: ['./opcion.component.scss']
})
export class OpcionComponent implements OnInit {

  @Input() carro: number;
  opcion: any;

  constructor(private lb: LoopbackService) { }

  ngOnInit() {
    this.onOpcion(this.carro);
  }

  onOpcion(id) {
    this.lb.getFindBy('opcionais', 'carroId', id)
    .subscribe(data => this.opcion = data);
  }

}
