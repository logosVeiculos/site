import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoopbackService } from '../../service/loopback.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  s: any = [
    { nome: '', ate: '', de: '', marca: '' }
  ];
  fmSearch: FormGroup;
  url: string;
  anos: any;
  marca: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private lb: LoopbackService
    ) { }

  ngOnInit() {
    this.getMarca();
    this.buildForm();
  }

  getMarca() {
    this.lb.getTable('marcas')
    .subscribe(data => this.marca = data);
  }

  onSearch() {
    this.lb.updatedSelection(this.fmSearch.value);
    this.router.navigate(['estoque']);
  }

  buildForm() {
    this.fmSearch = this.fb.group({
      marca: [''],
      de: [''],
      ate: [''],
      modelo: ['']
    });
  }
}
