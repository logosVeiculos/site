import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  flag: boolean;
  bdisplay: string;
  cdisplay: string;
  icon: string;
  constructor() { }

  ngOnInit() {
    this.flag = true;
    this.bdisplay = 'block';
    this.cdisplay = 'none';
    this.icon = 'bars';
  }


  onOpen() {
    this.flag = false;
    this.bdisplay = 'none';
    this.cdisplay = 'block';
    this.icon = 'times';
  }

  onClose() {
    this.flag = true;
    this.cdisplay = 'none';
    this.bdisplay = 'block';
    this.icon = 'bars';
  }
}
